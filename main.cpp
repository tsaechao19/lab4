#include <iostream>
#include "linked_list.h"

int main() {

    int arr[5] = {6,7,8,9,10};
    int size=sizeof(arr)/sizeof(arr[0]);
    linked_list list1(0);
    list1.append(1);
    list1.append(2);
    list1.append(3);
    list1.append(4);
    list1.append(5);
    list1.append(arr,size);
    list1.print();
    return 0;
}